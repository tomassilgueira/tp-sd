package client;

import java.io.*;
import java.net.*;

import py.una.entidad.Respuesta;
import py.una.entidad.RespuestaJSON;
import py.una.entidad.Solicitud;
import py.una.entidad.SolicitudJSON;

public class Client {

	public static void main(String[] args) throws IOException {

        Socket unSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;
        int timeOutConexion = 5000;
		int timeOutRecepcion = 5000;
        long ini = 0;
        long fin = 0;

        try {

            SocketAddress sockaddr = new InetSocketAddress("localhost", 4444);
            unSocket = new Socket();

            ini = System.currentTimeMillis();
            unSocket.connect(sockaddr, timeOutConexion);
            unSocket.setSoTimeout(timeOutRecepcion);

            // enviamos nosotros
            out = new PrintWriter(unSocket.getOutputStream(), true);
            //viene del servidor
            in = new BufferedReader(new InputStreamReader(unSocket.getInputStream()));

        }catch (SocketTimeoutException e){
            fin = System.currentTimeMillis();
            System.err.println("Fallo de Timeout de conexion en " + timeOutConexion);
            System.err.println("Duracion " + (fin-ini));
            System.exit(1);
        }catch (UnknownHostException e) {
            System.err.println("Host desconocido");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Error de I/O en la conexion al host");
            System.exit(1);
        }

        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        String input;
        boolean seguir = true;

        try {
	        while(seguir) {
	        	System.out.println("*********************************************************************");
	        	System.out.println("Opciones: 1- Consultar saldo. 2- Depositar. 3- Extraer. 4- Tranferir.");
	        	System.out.print("Elija una opcion > ");
	
	            input = stdIn.readLine();
	
	            Solicitud s = new Solicitud();
	            Respuesta r = new Respuesta();
	
	            if(input.trim().compareTo("1") == 0 ) { //Consultar
	        		s.setTipo(1);
	        		System.out.print("Ingresar ID de la cuenta > ");
	        		s.setInvocador( Integer.parseInt( stdIn.readLine() ) );
	        		s.setReceptor(0);
	        		System.out.print("Ingresar monto a consultar > ");
	        		s.setMonto( Integer.parseInt( stdIn.readLine() ) );

	        		out.println( SolicitudJSON.objetoString(s) );
	        		
	        		r = RespuestaJSON.stringObjeto( in.readLine() );
	
	            }else if(input.trim().compareTo("2") == 0 ) { //Depositar
	        		s.setTipo(2);
	        		System.out.print("Ingresar ID de la cuenta > ");
	        		s.setInvocador( Integer.parseInt( stdIn.readLine() ) );
	        		s.setReceptor(0);
	        		System.out.print("Ingresar monto a depositar > ");
	        		s.setMonto( Integer.parseInt( stdIn.readLine() ) );
	
	        		out.println( SolicitudJSON.objetoString(s) );
	        		r = RespuestaJSON.stringObjeto( in.readLine() );

	        	}else if(input.trim().compareTo("3") == 0 ) { //Extraer
	        		s.setTipo(3);
	        		System.out.print("Ingresar ID de la cuenta > ");
	        		s.setInvocador( Integer.parseInt( stdIn.readLine() ) );
	        		s.setReceptor(0);
	        		System.out.print("Ingresar monto a extraer > ");
	        		s.setMonto( Integer.parseInt( stdIn.readLine() ) );
	
	        		out.println( SolicitudJSON.objetoString(s) );
	        		r = RespuestaJSON.stringObjeto( in.readLine() );

	        	}else if(input.trim().compareTo("4") == 0 ) { //Transferir
	        		s.setTipo(4);
	        		System.out.print("Ingresar ID de la cuenta a extraer > ");
	        		s.setInvocador( Integer.parseInt( stdIn.readLine() ) );
	        		System.out.print("Ingresar ID de la cuenta receptora > ");
	        		s.setReceptor( Integer.parseInt( stdIn.readLine() ) );
	        		System.out.print("Ingresar monto a transferir > ");
	        		s.setMonto( Integer.parseInt( stdIn.readLine() ) );
	
	        		out.println( SolicitudJSON.objetoString(s) );
	        		r = RespuestaJSON.stringObjeto( in.readLine() );

	        	}else{
	        		seguir = false;
	        		r.setMensaje("Opcion invalida. Vai");
	        	}

	            System.out.println( r.getMensaje() );

	        }
		}catch(SocketTimeoutException exTime){
			/*Tiempo de espera agotado para recepcion de datos del servidor.*/
			Respuesta r = new Respuesta();
			r.setEstado(-1);
			r.setMensaje("Indefinido");
			r.setIdTransaccion(null);
			System.out.println(r.getMensaje() + " Comprobar si la transaccion se realizo con exito." );
		}catch(IOException e) {
            System.out.println("Error con la conexion a la base de datos.");
		}

        out.close();
        in.close();
        stdIn.close();
        unSocket.close();
    }
}
