package py.una.bd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import py.una.entidad.Transferencia;
import py.una.entidad.Respuesta;

public class TransferenciaDAO{
	/**
	 * @param tr: un objeto tipo Transferencia
	 * @return r: un objeto tipo Respuesta
	 * 				r.idTransaccion: indica el id de la transferencia realizada
	 * 				r.estado:   0 -> Transaccion exitosa
	 * 							2 -> Inexistencia de alguna de las cuentas proporcionadas, especificacion en el campo mensaje
	 * 							3 -> Error en la comprobacion de la existencia de las cuentas
	 * 							4 -> Error en la insercion de la transaccion en la BD
	 * 							5 -> Error al desconectar la BD
	 * 							6 -> Saldo insuficiente
	 * 							7 -> Error al comprobar el saldo
	 * 				r.mensaje: contiene un mensaje indicando el resultado de la transaccion
	 */	
	public static Respuesta transferir(Transferencia tr) {
		Respuesta r = new Respuesta();

		if(CuentaDAO.existeCuenta(tr.getIdEnvia())==1 && CuentaDAO.existeCuenta(tr.getIdRecibe())==1) { //si existen ambas cuentas
			if(CuentaDAO.tieneSaldoSuficiente(tr.getIdEnvia(), tr.getMonto())==1) {
				String SQL = "INSERT INTO TRANSFERENCIA (IdTransf, IdEnvia, IdRecibe, Monto) " + "VALUES(auto_id_tr(),?,?,?)";
			     
	            Connection conn = null;
	        	
	        	try 
	            {
	            	conn = Bd.connect();
	            	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
	                pstmt.setInt(1, tr.getIdEnvia());
	                pstmt.setInt(2, tr.getIdRecibe());
	                pstmt.setInt(3, tr.getMonto());
	     
	                int affectedRows = pstmt.executeUpdate();
	                // check the affected rows 
	                
	                if (affectedRows > 0) {
	                    // get the ID back
	                  try (ResultSet rs = pstmt.getGeneratedKeys()) {
	                        if (rs.next()) {
	                            r.setIdTransaccion(rs.getString(1));
	                            r.setEstado(0);
	                            r.setMensaje("Transferencia exitosa");
	                        }
	                    } catch (SQLException ex) {
	                        r.setMensaje(ex.getMessage());
	                    }
	                }
	            } catch (SQLException ex) {
	            	r.setMensaje("Error en la insercion: " + ex.getMessage());
	            	r.setEstado(4);
	            }
	            finally  {
	            	try{
	            		conn.close();
	            	}catch(Exception ef){
	            		r.setMensaje("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
	            		r.setEstado(5);
	            	}
	            }		
			}else if(CuentaDAO.tieneSaldoSuficiente(tr.getIdEnvia(), tr.getMonto())==0)//no tiene saldo suficiente para realizar la transferencia
		{
			r.setMensaje("Saldo insuficiente");
			r.setEstado(6);
		}
		else//no se pudo comprobar el saldo
		{	
			r.setMensaje("Error al comprobar el saldo de la cuenta");
			r.setEstado(7);
		}
		
	}else if (CuentaDAO.existeCuenta(tr.getIdEnvia())==0)//no existe la cuenta emisora
	{
		r.setMensaje("No existe la cuenta emisora");	
		r.setEstado(2);
	}else if (CuentaDAO.existeCuenta(tr.getIdRecibe())==0)//no existe la cuenta receptora
	{
		r.setMensaje("No existe la cuenta receptora");	
		r.setEstado(2);
	}
	else//no se pudo comprobar la existencia
	{	
		r.setMensaje("Error al comprobar existencia de la cuenta ingresada");	
		r.setEstado(3);
	}
	return r;
				
	}
}
