package py.una.bd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import py.una.entidad.Respuesta;

import py.una.entidad.Deposito;
import py.una.bd.CuentaDAO;

public class DepositoDAO {
/**
 * @param d: un objeto tipo Deposito
 * @return r: un objeto tipo Respuesta
 * 				r.idTransaccion: indica el id del deposito realizado
 * 				r.estado:   0 -> Transaccion exitosa
 * 							2 -> No Existe la cuenta proporcionada
 * 							3 -> Error en la comprobacion de la existencia de la cuenta
 * 							4 -> Error en la insercion de la transaccion en la BD
 * 							5 -> Error al desconectar la BD
 * 				r.mensaje: contiene un mensaje indicando el resultado de la transaccion
 */
	public static Respuesta depositar(Deposito d) {
		Respuesta r = new Respuesta();

        if(CuentaDAO.existeCuenta(d.getIdCuenta())==1) //comprueba si existe la cuenta en la que queremos depositar
        {
        	
            String SQL = "INSERT INTO DEPOSITO(IdDeposito, IdCuenta, Monto) " + "VALUES(auto_id_de(),?,?)";
     
            Connection conn = null;
        	
        	try 
            {
            	conn = Bd.connect();
            	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
                pstmt.setInt(1, d.getIdCuenta());
                pstmt.setInt(2, d.getMonto());
     
                int affectedRows = pstmt.executeUpdate();
                // check the affected rows 
                
                if (affectedRows > 0) {
                    // get the ID back
                  try (ResultSet rs = pstmt.getGeneratedKeys()) {
                        if (rs.next()) {
                            r.setIdTransaccion(rs.getString(1));
                            r.setEstado(0);
                            r.setMensaje("Transaccion exitosa");
                        }
                    } catch (SQLException ex) {
                        //System.out.println(ex.getMessage());
                        r.setMensaje(ex.getMessage());
                    }
                }
            } catch (SQLException ex) {
            	r.setMensaje("Error en la insercion: " + ex.getMessage());
            	r.setEstado(4);
                //System.out.println("Error en la insercion: " + ex.getMessage());
            }
            finally  {
            	try{
            		conn.close();
            	}catch(Exception ef){
            		r.setMensaje("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
            		r.setEstado(5);
            		//System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
            	}
            }	
        }else if (CuentaDAO.existeCuenta(d.getIdCuenta())==0)//no existe la cuenta 
	{
		r.setEstado(2);
		r.setMensaje("No existe la cuenta ingresada para el deposito");	
	}
	else//no se pudo comprobar la existencia
	{	
		r.setEstado(3);
		r.setMensaje("Error al comprobar existencia de la cuenta ingresada");
	}
        return r;
	}
 
 }