package py.una.bd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import py.una.entidad.Cuenta;


public class CuentaDAO {
	/*@param idCuenta de la cuenta que queremos saber si existe
	 *@return 
	 *0 si no existe la cuenta
	 *1 si existe la cuenta
	 *2 si ocurrio un error al consultar la bd
	 * */
	public static int existeCuenta(int idCuenta) {

		String SQL = "SELECT IdCuenta, saldo FROM CUENTA WHERE IdCuenta = ? "; //hago una consulta cualquiera
		
		int respuesta = 2;
		
		Connection conn = null;
        
		try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setInt(1, idCuenta);
        	
        	ResultSet rs = pstmt.executeQuery();
            respuesta = 0;
        	if(rs.next()) {
        			respuesta = 1;	//respuesta es true si existe un resultado a esa consulta, con ese id de cuenta
        	}
        	
        } catch (SQLException ex) {
            System.out.println("Error en la seleccion: " + ex.getMessage());

        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return respuesta;

	}
	/**
	 *@param idCuenta de la cuenta y Monto a consultar
	 *@return 	 
	 *0 si no tiene el saldo suficiente
	 *1 si tiene el saldo suficiente
	 *2 si ocurrio un error al consultar la bd
	 */
	public static int tieneSaldoSuficiente(int idc, int n) {

		String SQL = "SELECT IdCuenta, Saldo FROM CUENTA WHERE IdCuenta = ? "; //hago una consulta cualquiera
		
		int respuesta = 2;
		
		Connection conn = null; 
        
		try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setInt(1, idc);
        	
        	ResultSet rs = pstmt.executeQuery();
        	respuesta = 0;
        	
        	while(rs.next()) {
        		Cuenta x = new Cuenta();
        		x.setIdCuenta(rs.getInt(1));
        		x.setSaldo(rs.getInt(2));
        		if(n<=x.getSaldo()) {
        			respuesta = 1;
        		}
        	}
        	
        } catch (SQLException ex) {
            System.out.println("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return respuesta;
	}
}