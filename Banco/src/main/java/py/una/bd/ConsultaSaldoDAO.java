package py.una.bd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import py.una.bd.CuentaDAO;
import py.una.entidad.Consulta;
import py.una.entidad.Respuesta;

public class ConsultaSaldoDAO {
	/**
	 * @param c: un objeto tipo Consulta
	 * @return r: un objeto tipo Respuesta
	 * 				r.idTransaccion: indica el id de la consulta realizada
	 * 				r.estado:   0 -> Transaccion exitosa
	 * 							2 -> No Existe la cuenta proporcionada
	 * 							3 -> Error en la comprobacion de la existencia de la cuenta
	 * 							4 -> Error en la insercion de la transaccion en la BD
	 * 							5 -> Error al desconectar la BD
	 * 							6 -> Saldo insuficiente
	 * 							7 -> Error al comprobar el saldo
	 * 				r.mensaje: contiene un mensaje indicando el resultado de la transaccion
	 */	
	public static Respuesta consultar(Consulta c) {
		Respuesta r = new Respuesta();

		if(CuentaDAO.existeCuenta(c.getIdCuenta())==1) {		//si existe la cuenta
			String SQL = "INSERT INTO CONSULTASALDO (IdConsulta, IdCuenta, Monto) " + "VALUES(auto_id_co(),?,?)";
		     if(CuentaDAO.tieneSaldoSuficiente(c.getIdCuenta(), c.getMonto())==1) {
	            Connection conn = null;
	        	
	        	try 
	            {
	            	conn = Bd.connect();
	            	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
	                pstmt.setInt(1, c.getIdCuenta());
	                pstmt.setInt(2, c.getMonto());
	     
	                int affectedRows = pstmt.executeUpdate();
	                // check the affected rows 
	                
	                if (affectedRows > 0) {
	                    // get the ID back
	                  try (ResultSet rs = pstmt.getGeneratedKeys()) {
	                        if (rs.next()) {
	                            r.setIdTransaccion(rs.getString(1));
	                            r.setEstado(0);
	                            r.setMensaje("Saldo Suficiente");
	                        }
	                    } catch (SQLException ex) {
	                        //System.out.println(ex.getMessage());
	                        r.setMensaje(ex.getMessage());
	                    }
	                }
	            } catch (SQLException ex) { 
	            	r.setMensaje("Error en la insercion: " + ex.getMessage());
	            	r.setEstado(4);
	               // System.out.println("Error en la insercion: " + ex.getMessage());
	            }
	            finally  {
	            	try{
            		conn.close();
            		}catch(Exception ef){ 
            			r.setMensaje("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
            			r.setEstado(5);
            			//System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
            		}
            	}
        	}else if(CuentaDAO.tieneSaldoSuficiente(c.getIdCuenta(), c.getMonto())==0) //no tiene el saldo suficiente
        	{
        		r.setMensaje("Saldo insuficiente");
        		r.setEstado(6);
        	}else 
        	{
        		r.setMensaje("Error al comprobar el saldo de la cuenta");
				r.setEstado(7);
        	}
		}else if (CuentaDAO.existeCuenta(c.getIdCuenta())==0)//no existe la cuenta 
		{
			r.setMensaje("No existe la cuenta ingresada para la consulta");	
			r.setEstado(2);
		}
		else//no se pudo comprobar la existencia
		{	
			r.setMensaje("Error al comprobar existencia de la cuenta ingresada");
			r.setEstado(3);
		}
		return r;
	}
}