package py.una.server.tcp;

import java.net.*;
import java.io.*;

public class ServerGral {

	public static void main(String[] args) throws IOException {

		ServerSocket serverSocket = null;
        boolean listening = true;

        try {
            serverSocket = new ServerSocket(4444);
        } catch (IOException e) {
            System.err.println("No se puede abrir el puerto: 4444.");
            System.exit(1);
        }
        System.out.println("Puerto abierto: 4444.");

        while (listening) {
            new ServerHilo( serverSocket.accept() ).start();
        }
        serverSocket.close();
    }
}
