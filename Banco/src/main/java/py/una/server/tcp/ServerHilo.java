package py.una.server.tcp;

import java.net.*;

import py.una.entidad.Consulta;
import py.una.bd.ConsultaSaldoDAO;
import py.una.entidad.Deposito;
import py.una.bd.DepositoDAO;
import py.una.entidad.Extraccion;
import py.una.bd.ExtraccionDAO;
import py.una.bd.TransferenciaDAO;
import py.una.entidad.Respuesta;
import py.una.entidad.RespuestaJSON;
import py.una.entidad.Solicitud;
import py.una.entidad.SolicitudJSON;
import py.una.entidad.Transferencia;

import java.io.*;

public class ServerHilo extends Thread {
    private Socket socket = null;

    public ServerHilo(Socket socket) {
        super("ServerHilo");
        this.socket = socket;
    }

    public void run() {

        try {
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader( new InputStreamReader( socket.getInputStream() ) );

            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                System.out.println("Mensaje recibido: " + inputLine);

                Solicitud s = new Solicitud();
                Respuesta r = new Respuesta();

                try {
                	s = SolicitudJSON.stringObjeto( inputLine );
                }catch(Exception e) {
                	System.err.println(e.getMessage() );
                }

                if( s.getTipo() == 1 ) { //Consultar saldo
                	Consulta c = new Consulta();
                	c.setIdCuenta( s.getInvocador() );
                	c.setMonto( s.getMonto() );

                	r = ConsultaSaldoDAO.consultar(c);
                	out.println( RespuestaJSON.objetoString(r) );

                }else if( s.getTipo() == 2 ) { //Depositar
                	Deposito d = new Deposito();
                	d.setIdCuenta( s.getInvocador() );
                	d.setMonto( s.getMonto() );

                	r = DepositoDAO.depositar(d);
                	out.println( RespuestaJSON.objetoString(r) );

                }else if( s.getTipo() == 3 ) { //Extraccion
                	Extraccion e = new Extraccion();
                	e.setIdCuenta( s.getInvocador() );
                	e.setMonto( s.getMonto() );

                	r = ExtraccionDAO.extraer(e);
                	out.println( RespuestaJSON.objetoString(r) );

                }else if( s.getTipo() == 4 ) { //Transferencia
                	Transferencia t = new Transferencia();
                	t.setIdEnvia( s.getInvocador() );
                	t.setIdRecibe( s.getReceptor() );
                	t.setMonto( s.getMonto() );

                	r = TransferenciaDAO.transferir(t);
                	out.println( RespuestaJSON.objetoString(r) );
                }else
                	out.println("Opcion no valida"); //Esto no deberia ocurrir
            }

            out.close();
            in.close();
            socket.close();
            System.out.println("Finalizando Hilo");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
