package py.una.entidad;

public class Extraccion
{
	String idExtraccion;
	int idCuenta;
	int monto;
	
	public String getId() {
		return this.idExtraccion;
	}
	
	public void setId(String id) {
		this.idExtraccion = id;
	}

	public int getIdCuenta() {
		return this.idCuenta;
	}
	
	public void setIdCuenta(int id) {
		this.idCuenta = id;
	}
	
	public int getMonto() {
		return this.monto;
	}
	
	public void setMonto(int monto) {
		this.monto = monto;
	}
}
