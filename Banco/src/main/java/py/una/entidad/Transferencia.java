package py.una.entidad;


public class Transferencia
{
	String idTranf;
	int idEnvia;
	int idRecibe;
	int monto;

	public String getId() {
		return this.idTranf;
	}

	public void setId(String id) {
		this.idTranf = id;
	}

	public int getIdEnvia() {
		return this.idEnvia;
	}
	
	public void setIdEnvia(int id) {
		this.idEnvia = id;
	}

	public int getIdRecibe() {
		return this.idRecibe;
	}
	
	public void setIdRecibe(int id) {
		this.idRecibe = id;
	}

	public int getMonto() {
		return this.monto;
	}
	
	public void setMonto(int monto) {
		this.monto = monto;
	}
}
