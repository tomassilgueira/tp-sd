package py.una.entidad;


public class Consulta
{
	String idConsulta;
	int idCuenta;
	int monto;

	public String getId() {
		return this.idConsulta;
	}
	
	public void setId(String id) {
		this.idConsulta = id;
	}

	public int getIdCuenta() {
		return this.idCuenta;
	}
	
	public void setIdCuenta(int id) {
		this.idCuenta = id;
	}
	
	public int getMonto() {
		return this.monto;
	}
	
	public void setMonto(int monto) {
		this.monto = monto;
	}
}
