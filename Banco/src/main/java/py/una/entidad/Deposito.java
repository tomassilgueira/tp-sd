package py.una.entidad;


public class Deposito
{
	String idDeposito;
	int idCuenta;
	int monto;

	public String getId() {
		return this.idDeposito;
	}
	
	public void setId(String id) {
		this.idDeposito = id;
	}

	public int getIdCuenta() {
		return this.idCuenta;
	}
	
	public void setIdCuenta(int id) {
		this.idCuenta = id;
	}
	
	public int getMonto() {
		return this.monto;
	}
	
	public void setMonto(int monto) {
		this.monto = monto;
	}
}
