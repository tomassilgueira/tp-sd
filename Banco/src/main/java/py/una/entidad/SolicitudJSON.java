package py.una.entidad;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class SolicitudJSON {

    public static String objetoString(Solicitud s) {	

		JSONObject obj = new JSONObject();

		obj.put("tipo", s.getTipo());
        obj.put("invocador", s.getInvocador());
        obj.put("receptor", s.getReceptor());
        obj.put("monto", s.getMonto());

        return obj.toJSONString();
    }

    public static Solicitud stringObjeto(String str) throws Exception {

        JSONParser parser = new JSONParser();

        Object obj = parser.parse(str.trim());
        JSONObject jsonObj = (JSONObject) obj;

        Solicitud s = new Solicitud();
        s.setTipo     ( ((Long)jsonObj.get("tipo")     ).intValue() );
        s.setInvocador( ((Long)jsonObj.get("invocador")).intValue() );
        s.setReceptor ( ((Long)jsonObj.get("receptor") ).intValue() );
        s.setMonto    ( ((Long)jsonObj.get("monto")    ).intValue() );

        return s;
	}
}
