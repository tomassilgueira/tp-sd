package py.una.entidad;

public class Cuenta
{
	int id;
	int saldo;
	
	public int getIdCuenta() {
		return this.id;
	}
	
	public void setIdCuenta(int newId) {
		this.id = newId;
	}
	
	public int getSaldo() {
		return this.saldo;
	}
	
	public void setSaldo(int newSaldo) {
		this.saldo = newSaldo;
	}
}
