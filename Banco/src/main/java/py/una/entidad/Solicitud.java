package py.una.entidad;

public class Solicitud {

	int tipo;
	int invocador;
	int receptor;
	int monto;

	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public int getInvocador() {
		return invocador;
	}
	public void setInvocador(int invocador) {
		this.invocador = invocador;
	}
	public int getReceptor() {
		return receptor;
	}
	public void setReceptor(int receptor) {
		this.receptor = receptor;
	}
	public int getMonto() {
		return monto;
	}
	public void setMonto(int monto) {
		this.monto = monto;
	}
	
}
