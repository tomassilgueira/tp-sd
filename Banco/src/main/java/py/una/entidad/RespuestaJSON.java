package py.una.entidad;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class RespuestaJSON {

    public static String objetoString(Respuesta r) {

		JSONObject obj = new JSONObject();
		obj.put("idTransaccion", r.getIdTransaccion() );
		obj.put("estado", r.getEstado() );
		obj.put("mensaje", r.getMensaje() );

        return obj.toJSONString();
    }

    public static Respuesta stringObjeto(String str) {

    	Respuesta r = new Respuesta();
    	try {
	        JSONParser parser = new JSONParser();
	
	        Object obj = parser.parse(str.trim());
	        JSONObject jsonObj = (JSONObject) obj;

	        r.setMensaje((String) jsonObj.get("mensaje"));
	        r.setIdTransaccion( (String) jsonObj.get("idTransaccion"));
	        r.setEstado(((Long) jsonObj.get("estado")).intValue());
	        
    	}catch(Exception ex) {
    		//r.setEstado(777);
    	}
        return r;
	}
}
