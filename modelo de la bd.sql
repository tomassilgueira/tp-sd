--TABLAS

CREATE TABLE CUENTA (
	IdCuenta int not null,
	Saldo int not null,
	CONSTRAINT pkc PRIMARY KEY (IdCuenta)
);

CREATE TABLE CONSULTASALDO(
	IdConsulta varchar(10),
	IdCuenta int not null,
	Monto int not null,
	CONSTRAINT pkcs PRIMARY KEY (IdConsulta),
	CONSTRAINT fkcd FOREIGN KEY (IdCuenta) REFERENCES CUENTA (IdCuenta)
);

CREATE TABLE TRANSFERENCIA (
	IdTransf varchar(10),
	IdEnvia int not null,
	IdRecibe int not null,
	Monto int not null,
	CONSTRAINT pkt PRIMARY KEY (IdTransf),
	CONSTRAINT fke FOREIGN KEY (IdEnvia) REFERENCES CUENTA (IdCuenta),
	CONSTRAINT fkr FOREIGN KEY (IdRecibe) REFERENCES CUENTA (IdCuenta)
);

CREATE TABLE DEPOSITO (
	IdDeposito varchar(10),
	IdCuenta int not null,
	Monto int not null,
	CONSTRAINT pkd PRIMARY KEY (IdDeposito),
	CONSTRAINT fkcd FOREIGN KEY (IdCuenta) REFERENCES CUENTA (IdCuenta)
);

CREATE TABLE EXTRACCION (
	IdExtrac varchar(10),
	IdCuenta int not null,
	Monto int not null,
	CONSTRAINT pke PRIMARY KEY (IdExtrac),
	CONSTRAINT fkce FOREIGN KEY (IdCuenta) REFERENCES CUENTA (IdCuenta)
);

--FUNCIONES Y TRIGGERS PARA LAS TRANSACCIONES

CREATE FUNCTION deposito() RETURNS TRIGGER AS $disparador1$
begin
	update CUENTA set Saldo= Saldo+new.Monto where IdCuenta= new.IdCuenta;
	RETURN NULL;
end;
$disparador1$ LANGUAGE PLPGSQL;

CREATE TRIGGER disparador1 after insert on DEPOSITO
for each row
EXECUTE PROCEDURE deposito();


CREATE FUNCTION extraccion() RETURNS TRIGGER AS $disparador2$
begin
	update CUENTA set Saldo= Saldo-new.Monto where IdCuenta= new.IdCuenta;
	RETURN NULL;
end;
$disparador2$ LANGUAGE PLPGSQL;

CREATE TRIGGER disparador2 after insert on EXTRACCION
for each row
EXECUTE PROCEDURE extraccion();


CREATE FUNCTION transferencia1() RETURNS TRIGGER AS $disparador3$
begin
	update CUENTA set Saldo= Saldo-new.Monto where IdCuenta= new.IdEnvia;
	return null;
end;
$disparador3$ LANGUAGE PLPGSQL;

create TRIGGER disparador3 after insert on TRANSFERENCIA
for each row
EXECUTE PROCEDURE transferencia1();


CREATE FUNCTION transferencia2() RETURNS TRIGGER AS $disparador4$
begin
	update CUENTA set Saldo= Saldo+new.Monto where IdCuenta= new.IdRecibe;
	return null;
end;
$disparador4$ LANGUAGE PLPGSQL;

create TRIGGER disparador4 after insert on TRANSFERENCIA
for each row
EXECUTE PROCEDURE transferencia2();

--SECUENCIAS PARA LOS id´s

CREATE SEQUENCE seq_autoid_cu
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 200000000
  START 1;

CREATE SEQUENCE seq_autoid_co
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 200000000
  START 1;

CREATE SEQUENCE seq_autoid_de
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 200000000
  START 1;

CREATE SEQUENCE seq_autoid_ex
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 200000000
  START 1;

CREATE SEQUENCE seq_autoid_tr
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 200000000
  START 1;

--FUNCIONES PARA ASIGNACION DE id´s DE TRANSACCIONES  
  
create or replace function auto_id_cu () returns bigint as $$
select nextval('seq_autoid_cu')
$$ language sql;

create or replace function auto_id_co () returns varchar as $$
select 'c'||nextval('seq_autoid_co')
$$ language sql;

create or replace function auto_id_de () returns varchar as $$
select 'd'||nextval('seq_autoid_de')
$$ language sql;

create or replace function auto_id_ex () returns varchar as $$
select 'e'||nextval('seq_autoid_ex')
$$ language sql;

create or replace function auto_id_tr () returns varchar as $$
select 't'||nextval('seq_autoid_tr')
$$ language sql;
