Banco
=====

Requerimientos de instalacion
-----------------------------

-JavaSE version 1.7
-PostgreSQL version 10.3

Como crear la estructura de Base de datos
-----------------------------------------

Ejecutar script "modelo de la bd.sql"

Como poblar los datos iniciales necesarios de Base de datos
-----------------------------------------------------------
Ejecutar script "poblarBdBanco.sql"

Como levantar los componentes necesarios.
-----------------------------------------
Compilar y ejecutar el archivo ServerGral.java
Compilar y ejecutar el archivo Client.java

API de servicios ofrecidos
--------------------------
El servidor recibe un objeto Solicitud, cuyos campos son 
	-tipo 
	-invocador 
	-receptor 
	-monto
	
dependiendo del valor del campo tipo, el servidor ejecutara la funcion:
	(tipo = 1) -> consulta(idCuenta, Monto)
	(tipo = 2) -> depositar(idCuenta, Monto)
	(tipo = 3) -> extraer(idCuenta, Monto)
	(tipo = 4) -> transferir(idCuentaEmisora, idCuentaReceptora, Monto)
	
todas las funciones retornan un objeto Respuesta, cuyos campos son 
	-idTransaccion
	-estado
	-mensaje

valor del campo estado:
	estado = 0 -> Transaccion exitosa
	estado = 2 -> No Existe la cuenta proporcionada(en el caso de transferencia, la cuenta inexistente sera especificada en el campo mensaje)
	estado = 3 -> Error en la comprobacion de la existencia de la cuenta
	estado = 4 -> Error en la insercion de la transaccion en la BD
	estado = 5 -> Error al desconectar la BD
	estado = 6 -> Saldo insuficiente
	estado = 7 -> Error al comprobar el saldo
	estado = -1 -> Indefinido